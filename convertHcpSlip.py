#!/usr/bin/python

import numpy as np


def dir4two3(ind4):
    up = ind4[0]
    vp = ind4[1]
    t = ind4[2]
    w = ind4[3]
    u = (2.)*up +(1.)*vp
    v = (1.)*up + (2.)*vp
    #u = up - t
    #v = vp - t
    vec = [u,v,w]
    return vec

def plane4two3(ind4):
    up = ind4[0]
    vp = ind4[1]
    t = ind4[2]
    w = ind4[3]
    vec = [up, vp, w]
    return vec

#input 4 index
fourIndInput = 'tiSlipSystems.dat'

#output file will three indices
threeIndFile = 'tiSlipThreeInd.dat'
dataFile = 'tiSlipThreeData.dat'

ind = np.loadtxt(fourIndInput,delimiter=',')
nSlip =  ind.shape[0]
fileOutput = open(threeIndFile,'w')
fileOutput2 = open(dataFile,'w')
direction = ind[:,0:4]
system = ind[:,4:8]
d3 = np.zeros((nSlip,3))
s3 = np.zeros((nSlip,3))

#Calcualte 3 miller indices from 4
for i in range(nSlip):
    d = direction[i,:]
    s = system[i,:]
    if np.dot(d,s) != 0.0:
        print 'check dot product'
    #print str(dir4two3(d)) + ' ' + str(plane4two3(s))
    d3[i,:] = dir4two3(d)
    s3[i,:] = plane4two3(s)
    if np.dot(d3[i,:],s3[i,:]) != 0.0:
        print 'dech 4 index dot product'

# output three indices to fortran file
for j in range(nSlip):
    if j == 0:
        fileOutput.write('C.... Basal Slip System'+'\n' )
    elif j == 3:
        fileOutput.write('C.... Prism Slip System'+'\n' )
    elif j == 6:
        fileOutput.write('C.... Pyramid <a> Slip System'+'\n' )
    elif j == 12:
        fileOutput.write('C.... Pyramid <a+c> Slip System'+'\n' )

    fileOutput.write('C......................'+'\n' )
    #write slip systems y
    # and slip direction z
    for i in range(3):
        fileOutput.write('	y(' + str(i+1) + ',' + str(j+1) + ')=' + str(s3[j,i]) + '\n' )
        fileOutput2.write(str(s3[j,i]) + ', ')
    for i in range(3):
        fileOutput.write('	z(' + str(i+1) + ',' + str(j+1) + ')=' + str(d3[j,i]) + '\n' )
        fileOutput2.write(str(d3[j,i]))
        if i < 2:
            fileOutput2.write(', ')
    fileOutput2.write('\n')
fileOutput.close
fileOutput2.close
