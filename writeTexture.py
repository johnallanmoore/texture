#!/usr/bin/python

import numpy as np
import random

type = 'euler'
type = 'quaternion'


fileName = 'texture.txt'
f = open(fileName,'w')

numOrient = 1000
pi = np.pi

for i in range(numOrient):
    if type == 'euler':
        a = 2.0*pi*random.random()
        b = 2.0*pi*random.random()
        c = 2.0*pi*random.random()
        f.write(str(a) + ',' +  str(b) + ',' + str(c) + '\n')
    elif type == 'quaternion':
        q0 = random.random()
        q1 = random.random()
        q2 = random.random()
        q3 = random.random()
        qmag = np.sqrt(q0**2. + q1**2. +  q2**2. + q3**2.)
        q0 = q0/qmag
        q1 = q1/qmag
        q2 = q2/qmag
        q3 = q3/qmag       
        f.write(str(q0) + ',' +  str(q1) + ',' + str(q2) +  ',' + str(q3) + '\n')        
f.close()
