#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

direction = [0,-1,-1]
miller = [1,-1,1]

# load slip system
inputFile = 'tiSlipThreeData.dat' # plane, direction
data = np.loadtxt(inputFile,delimiter=',')
# sklip system to plot
system = 19
miller = data[system,0:3]
direction = data[system,3:6]

caRatio = 1.599

a1 = np.array([1.,0,0])
a2 = np.array([-0.5,8.66025404e-01,0])
a3 = np.array([0.0  , 0.0 , caRatio*1.0])

M = [np.transpose(a1),np.transpose(a2),np.transpose(a3)]
#M = [a1,a2,a3]

b1 = np.cross(a2,a3)/np.linalg.det(M)
b2 = np.cross(a3,a1)/np.linalg.det(M)
b3 = np.cross(a1,a2)/np.linalg.det(M)

print b1
print b2
print b3

pi = np.pi
r = 1.0
theta = np.array([0.,60.,120.,180.,240.,300.,0.])
theta = (pi/180.)*theta
x = r*np.cos(theta)
y = r*np.sin(theta)
z = 0.0
plt.figure(1)
plt.plot(x,y,'o-')
plt.figure(2)
ax = plt.axes(projection='3d')
ax.plot3D(x,y,z,'o-')
ax.plot3D(x,y,z+caRatio,'o-')

g = miller[0]*b1 + miller[1]*b2 + miller[2]*b3

xVec = [1,0,0]
yVec = [0,1,0]
zVec = [0,0,1]

d = -1.0*(direction[0]*a1 + direction[1]*a2 + direction[2]*a3)
print "d " + str(d)
print "g " + str(g)

#print direction[0] - direction[1]*np.sin((pi/180.)*30.)
#print direction[1]*np.cos((pi/180.)*30.)

# new direction (a prime)
ap = np.array([np.dot(d,xVec), np.dot(d,yVec),np.dot(d,zVec)])


plt.figure(1)
plt.plot([0,miller[0]],[0,miller[1]],'r')
plt.plot([0,a1[0]],[0,a1[1]],'g')
plt.plot([0,a2[0]],[0,a2[1]],'g')
plt.plot([0,g[0]],[0,g[1]],'b')
plt.plot([0,direction[0]],[0,direction[1]],'r--')
plt.plot([0,ap[0]],[0,ap[1]],'b--')
plt.xlim([-1,1])
plt.ylim([-1,1])
plt.axis('equal')

plt.figure(2)
ax.plot([0,miller[0]],[0,miller[1]],[0,miller[2]],'r')
ax.plot([0,g[0]],[0,g[1]],[0,g[2]],'b')
ax.plot([0,-direction[0]],[0,-direction[1]],[0,-direction[2]],'r--')
ax.plot([0,ap[0]],[0,ap[1]],[0,ap[2]],'b--')
ax.set_xlim(-1,1)
ax.set_ylim(-1,1)
ax.set_zlim(0,2)
ax.axis('equal')

# print the dot product of slip and normal
print np.dot(ap,g)




#plt.show()
